local S = minetest.get_translator(minetest.get_current_modname())

-- mcl_core namespace is taken here, as I believe obsidian shards and glass
-- belong there

-- obsidian glass
minetest.register_node(":mcl_core:obsidian_glass", {
	description = S("Obsidian Glass"),
	_doc_items_longdesc = S("Obsidian glass is a variant of glass that appears to be transparent, with only frames being visible."),
	drawtype = "glasslike_framed_optional",
	is_ground_content = false,
	tiles = {"default_obsidian_glass.png", "default_obsidian_glass_detail.png"},
	paramtype = "light",
	paramtype2 = "glasslikeliquidlevel",
	sunlight_propagates = true,
	stack_max = 64,
	groups = {handy=1, glass=1, building_block=1, material_glass=1},
	sounds = mcl_sounds.node_sound_glass_defaults(),
	drop = "",
	_mcl_blast_resistance = 1.2,
	_mcl_hardness = 0.9,
	_mcl_silk_touch_drop = true,
})

-- obsidian shard
minetest.register_craftitem(":mcl_core:obsidian_shard", {
	description = S("Obsidian Shard"),
	_doc_items_longdesc = S("Obsidian shards are small pieces of obsidian; they can be used to create obsidian and obsidian glass."),
	inventory_image = "default_obsidian_shard.png",
	stack_max = 64,
	groups = { craftitem=1 },
})

minetest.register_node(":mcl_core:obsidian_gravel", {
        description = S("Obsidian Gravel"),
        _doc_items_longdesc = S("This block is similar to gravel, but has pieces of obsidian in it."),
        tiles = {"mcl_core_obsidian_gravel.png"},
        is_ground_content = true,
        stack_max = 64,
        groups = {handy=1,shovely=1, falling_node=1, enderman_takable=1, building_block=1, material_sand=1},
        drop = {
                max_items = 1,
                items = {
                        {items = {"mcl_core:obsidian_shard"},rarity = 8},
                        {items = {"mcl_core:obsidian_gravel"}}
                }
        },
        sounds = mcl_sounds.node_sound_gravel_defaults(),
        _mcl_blast_resistance = 1.8,
        _mcl_hardness = 1.2,
        _mcl_silk_touch_drop = true,
        _mcl_fortune_drop = {
                [1] = {
                        max_items = 1,
                        items = {
                                {items = {"mcl_core:obsidian_shard"},rarity = 6},
                                {items = {"mcl_core:obsidian_gravel"}}
                        }
                },
                [2] = {
                        max_items = 1,
                        items = {
                                {items = {"mcl_core:obsidian_shard"},rarity = 3},
                                {items = {"mcl_core:obsidian_gravel"}}
                        }
                },
                [3] = "mcl_core:obsidian_shard",
        },
})


-- crafting recipes
minetest.register_craft({
	output = "mcl_core:obsidian_shard 9",
	recipe = {
		{"mcl_core:obsidian"},
	}
})

minetest.register_craft({
	output = "mcl_core:obsidian",
	recipe = {
		{"mcl_core:obsidian_shard", "mcl_core:obsidian_shard", "mcl_core:obsidian_shard"},
		{"mcl_core:obsidian_shard", "mcl_core:obsidian_shard", "mcl_core:obsidian_shard"},
		{"mcl_core:obsidian_shard", "mcl_core:obsidian_shard", "mcl_core:obsidian_shard"},
	}
})

minetest.register_craft({
	type = "cooking",
	output = "mcl_core:obsidian_glass",
	recipe = "mcl_core:obsidian_shard",
	cooktime = 10,
})
